

def calcular_promedios(notas: list):
    
    def obtener_promedio(list_notas: list):
        return (list_notas[0]+list_notas[1])/2
    
    for hijo in notas:
        promedio = obtener_promedio(hijo)
    
    def mostrar_promedio():
        print(promedio)
    
    mostrar_promedio()
    


notas = [
    [4.5, 4.2],
    [4.1, 4.2]
]

""" referencia_funcion = calcular_promedios

referencia_funcion(notas) """

#calcular_promedios(notas)

def sumar(n1, n2):
    return n1+n2

suma = lambda n1,n2 : n1+n2

print( suma(20, 30) )

def calcular_promedio_con_lambda(notas: list):

    obtener_promedio = lambda lista: (lista[0]+lista[1])/2

    for notas_hijo in notas:
        print(obtener_promedio(notas_hijo))

calcular_promedio_con_lambda(notas)