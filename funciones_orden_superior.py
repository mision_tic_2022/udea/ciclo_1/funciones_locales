

def funcion_orden_superior(suma):
    print(suma(10,20))

suma = lambda n1,n2: n1+n2
funcion_orden_superior(suma)

def calculadora(n1,n2, operador):
    suma = lambda n1,n2: n1+n2
    resta = lambda n1,n2: n1-n2
    multiplicar = lambda n1,n2: n1*n2
    if operador == '+': 
        print(suma(n1,n2))
    elif operador == '-': 
        print(resta(n1,n2))
    elif operador == '*':
        print(multiplicar(n1,n2))